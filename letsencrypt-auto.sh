#!/bin/bash

domains=$1

if [ -z $domains ]
	then echo "Run it like this: sudo ./letsencrypt-auto.sh mydomain.com"

else

	#Stop webservers
	sudo service nginx stop
	sudo service apache2 stop

	#Renew
	sudo letsencrypt --renew-by-default certonly --standalone --domains $domains

	#Start webservers
	sudo service apache2 start
	sudo service nginx start

fi

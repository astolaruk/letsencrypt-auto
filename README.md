# letsencrypt-auto.sh

Simple script to stop webservers and do a Let's Encrypt cert issue or renewal.

Usage:

```sudo ./letsencrypt-auto mydomain.com```
